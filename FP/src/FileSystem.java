/**
 * File System for CSS 430 Final Project. This file system supports the 8 spec'd
 * system calls for the final project.
 * 
 * Returns -1 for error calls
 * 
 * NOTE: FTE - FileTableEntry
 * 
 * @author Alexander Auxier
 */

public class FileSystem
{
    // Init vars for our needed file system structures
    private SuperBlock curSBlock;
    private Directory curDir;
    private FileTable curFTable;

    // Seek method vars provided via assign spec
    private final int SEEK_SET = 0;
    private final int SEEK_CUR = 1;
    private final int SEEK_END = 2;

    /**
     * Constructor for the FileSystem. Here we will initialize our superblock,
     * directory, and our filetable The constructor requires a specified integer
     * value for the disk size to accomplish this initialization.
     */
    public FileSystem(int newSize)
    {
        // Init superblock, directory, and file table when recieving new size param
        curSBlock = new SuperBlock(newSize);
        curDir = new Directory(newSize);
        curFTable = new FileTable(curDir);

        FileTableEntry newEntry = open("/", "r"); // Make a new file table entry for opening root

        int initialzer = fsize(newEntry); // Use variable to check size

        if(initialzer > 0) // Check if current file is valid and init directory
        {
            byte[] initBuffer = new byte[initialzer]; // Use the size to create a byte buffer

            read(newEntry, initBuffer); // Read the in the file
            curDir.bytes2directory(initBuffer); // Init directory with byte buffer
        }

        close(newEntry); // Close the file after init
    }

    /**
     * Method stub provided in additional materials section of Final Project
     * assignment page. Decided to implement
     * 
     * This method deallocates block for the current file table entry. Used to check
     * for valid open when opening a file.
     */
    private boolean deallocAllBlocks(FileTableEntry curFTE)
    {
        // Check if there is more than one file table entry for the current entry
        if(curFTE.getInodeCount() != 1)
            return false; // Break and cancel if there is multiple pointers

        // Loop over current FTE direct inode pointers and return blocks
        for(short i = 0; i < 11; i++)
        {
            if(curFTE.inode.direct[i] != -1) // Check direct pointers for valid pointer
            {
                curSBlock.returnBlock(i); // Return block to freeList
                curFTE.inode.direct[i] = -1; // Reset direct pointers
            }
        }

        byte[] fileBuffer = null; // Byte buffer for current FTE inode

        // Check for unregistered index blocks on the curret FTE inode
        if(curFTE.inode.indirect >= 0) // If indirect pointer is valid, continue
        {
            fileBuffer = new byte[512]; // Make a new buffer

            SysLib.rawread(curFTE.inode.indirect, fileBuffer); // Read into the buffer
            curFTE.inode.indirect = -1; // Reset pointers
        }

        // Error condition
        else
            SysLib.cerr("Dealloc - Index Block Error");

        // Begin dealloc main
        if(fileBuffer != null)
        {
            // Init iterators to use jfor deallocing blocks
            int itr = 0; // Set here
            int nextItr; // Set later

            // Loop while next iteration is not negative
            while((nextItr = SysLib.bytes2short(fileBuffer, itr)) != -1)
                curSBlock.returnBlock(nextItr); // Return block at index
        }

        // Update current FTE inode with current iNumber
        curFTE.inode.toDisk(curFTE.iNumber);

        return true; // Dealloc complete
    }

    /**
     * This method destroys the file specified by fileName.
     */
    public int delete(String fileName)
    {
        FileTableEntry aFileDesc = open(fileName, "w"); // mode w should delete all blocks in order to write from

        int isClosed = close(aFileDesc); // attempt to close

        if(curDir.ifree(aFileDesc.iNumber) == true && isClosed == 0) // Has it been freed and deleted?
            return 0; // True

        return -1; // False
    }

    /**
     * Sync method for the FileSystem Directory contents are written to the current
     * file that has been opened This also calls the SuperBlock sync method
     */
    public void sync()
    {
        FileTableEntry current = open("/", "w"); // Open new FTE with write args
        byte[] fBuffer = curDir.directory2bytes(); // Convert directory into bytes

        write(current, fBuffer); // Send directory to current file
        close(current); // Close writes to file

        curSBlock.sync(); // Sync the superblock
    }

    /**
     * This method will be implement the Syslib.format(x) function for the
     * FileSystem. The contents of the DISK are cleared here.
     */
    public int format(int maxFiles)
    {
        if(curFTable.fempty())
            return -1;

        // Loop while the local FileTable is not empty
        while(!curFTable.fempty())
        {
            curSBlock.format(maxFiles); // Call Superblock format helper to clear superblock
            curDir = new Directory(curSBlock.totalInodes); // Create new directory for current superblock
            curFTable = new FileTable(curDir); // Make a new filetable for the formatted superblock and directory
        }

        return 0; // Completed signal
    }

    /**
     * File size getter. Returns the file size for a FileTableEntry via
     * getFileEntrySize method.
     */
    public int fsize(FileTableEntry fileDesc)
    {
        // Sync and return
        synchronized(fileDesc)
        {
            return fileDesc.getFileEntrySize(); // Use class specific getter on return
        }
    }

    /**
     * This method closes a file, commits all transactions, and unregisters fileDesc
     */
    public int close(FileTableEntry fileDesc)
    {
        synchronized(fileDesc)
        {
            fileDesc.count--;

            if(fileDesc.count > 0)
                return 0; // success
        }

        if(curFTable.ffree(fileDesc) == true)
            return 0; // success

        else
            return -1; // failure
    }

    /**
     * Opens the file specified by the String curFile name. Opmode specifies which
     * operation to perform
     */
    public FileTableEntry open(String curFile, String opMode)
    {
        // Valid File Name or OPmode check
        if(curFile == "" || opMode == "")
            return null;

        // Acquire new target file to open
        FileTableEntry targetEntry = curFTable.falloc(curFile, opMode);

        if(opMode == "w") // Check for write only operation
        {
            if(deallocAllBlocks(targetEntry) == false) // Break if multiple pointers
                return null;
        }

        return targetEntry; // Return target
    }

    /**
     * This method updates the seek pointer that corresponds to fileDesc
     * 
     * Reminder: SEEK_SET - offset from beginning of file SEEK_CUR - current
     * position plus offset SEEK_END - size of file plus offset
     */
    public int seek(FileTableEntry fileDesc, int offset, int position)
    {
        synchronized(fileDesc)
        {
            if(position == SEEK_SET)
                fileDesc.seekPtr = offset;

            else if(position == SEEK_CUR)
                fileDesc.seekPtr += offset;

            else if(position == SEEK_END)
                fileDesc.seekPtr = offset + fileDesc.inode.curFsize;

            else // Not a valid seek pointer (for our use)
                return -1;

            if(fileDesc.seekPtr < 0) // seek pointer is less than 0
                fileDesc.seekPtr = 0; // "clamp it to zero" aka set it to 0

            if(fileDesc.seekPtr > fileDesc.inode.curFsize) // seek pointer is beyond the size file
                fileDesc.seekPtr = fileDesc.inode.curFsize; // set seek pointer to EOF

            return fileDesc.seekPtr;
        }
    }

    /**
     * Write will write the contents of the input byte buffer to the input file that
     * has been passed in. These writes start based on the seek pointers. Data can
     * be overwritten or appended to files.
     * 
     * Return value should be number or bytes written, or negative for error
     * Negative values are based on the switch case values for error types. Error
     * numbers are provide via a helper method in Inode.java
     */
    public int write(FileTableEntry fileDesc, byte[] fileBuffer)
    {
        // Check if our opmode is read
        if(fileDesc.mode == "r")
            return -1; // Error on read for write operation

        // Sync threads before operation
        synchronized(fileDesc)
        {
            int wIndex1, wIndex2; // Init write index variables to use for looping
            wIndex1 = 0; // Index 1 will be mutated to return number of bytes written
            wIndex2 = fileBuffer.length; // Index 2 will be our buffer length

            // Loop while our upper bound index(wIndex2) is still > 0
            while(wIndex2 > 0)
            {
                // Init variable for getting our current target block from our file
                int curTBlock = fileDesc.inode.findTargetBlock(fileDesc.seekPtr);

                // Check if the current target block is invalid.
                if(curTBlock == -1) // If -1 then we must decide on next output
                {
                    int curFreeBlock = curSBlock.getFreeBlock(); // Get the next free block

                    /*
                     * Init our write context switch here We will use the Inode.java helper
                     * function, registerTargetBlock, to generate return cases for our write
                     */
                    switch(fileDesc.inode.registerTargetBlock(fileDesc.seekPtr, (short) curFreeBlock))
                    {
                        case 0: // Error case 0 from Inode.java
                            break; // Exit and continue
                        case -1: // Error case -1 from Inode.java
                            SysLib.cerr("SysMessage: Error on WRITE \n");
                            return -1; // Send error int back
                        case -2: // Error case -2. Ignore and Re-iterate
                        case -3: // Error case -3
                            // Set a new free block short based on the localSuperBlock
                            short newFreeBlock = (short) curSBlock.getFreeBlock();
                            // Check if the file is not registered at the index
                            if(!fileDesc.inode.registerIndexBlock(newFreeBlock)) // This causes invalid write
                            {
                                SysLib.cerr("SysMessage: Error on WRITE \n");
                                return -1; // Send error int back
                            }
                            // Check if the file registered at block, will cause invalid write otherwise
                            if(fileDesc.inode.registerTargetBlock(fileDesc.seekPtr, (short) curFreeBlock) != 0)
                            {
                                SysLib.cerr("SysMessage: Error on WRITE \n");
                                return -1; // Send error int back
                            }
                            break; // Exit case -3
                    }

                    curTBlock = curFreeBlock; // Reset the target block value with our superblock value
                }

                byte[] newWriteBuffer = new byte[512]; // Init a new byte buffer

                // Check if file is readable at the current target block
                if(SysLib.rawread(curTBlock, newWriteBuffer) == -1)
                    System.exit(2); // Exit write, error found

                // Init a destination pointer from the seek pointer to copy buffers
                int writePtr = fileDesc.seekPtr % 512;

                // Init new write indices/bound integers
                int nextWIndex, nextWIndex2; // Used for copying and write mutatiion
                nextWIndex = 512 - writePtr;
                nextWIndex2 = Math.min(nextWIndex, wIndex2);

                // Copy the buffers
                System.arraycopy(fileBuffer, wIndex1, newWriteBuffer, writePtr, nextWIndex2);

                // Write to the current target file on the block
                SysLib.rawwrite(curTBlock, newWriteBuffer);

                fileDesc.seekPtr += nextWIndex2; // Iterate the source file seekPtr

                // Change our write index integers for loop re-iteration
                wIndex1 += nextWIndex2;
                wIndex2 -= nextWIndex2;

                // Check if we have our inode pointer greater than size
                if(fileDesc.seekPtr > fileDesc.inode.curFsize)
                    fileDesc.inode.curFsize = fileDesc.seekPtr; // Update length
            }

            fileDesc.inode.toDisk(fileDesc.iNumber); // Write the inode contents back to disk

            return wIndex1; // Return wIndex1 as our num bytes written
        }
    }

    /**
     * Reads based on the length of the file buffer provided, from the provided
     * FileTableEntry. Successful read provides the number of bytes that have been
     * read, and -1 is used for read errors.
     * 
     * Position variables curPos and curPos2 track bytes read, with curPos being our
     * return value.
     */
    public int read(FileTableEntry fileDesc, byte[] fileBuffer)
    {
        // Check for invalid opmodes, looking for r
        if((fileDesc.mode == "w") || (fileDesc.mode == "a"))
            return -1; // Through error integer for invalid opmodes

        int curPos, curPos2; // Init loop position variables for loop iteration on read
        // Set values
        curPos = 0; // Will be used for returning current number of bytes read
        curPos2 = fileBuffer.length;

        // Synchronize before read functions perform
        synchronized(fileDesc)
        {
            // Loop based on our outer position (curPos2) and the input fileDesc seek
            // pointer
            while(curPos2 > 0 && fileDesc.seekPtr < fsize(fileDesc))
            {
                int target = fileDesc.inode.findTargetBlock(fileDesc.seekPtr);

                // If we get a -1, break due to error int recieved
                if(target == -1)
                    break;

                byte[] newBuffer = new byte[512]; // Init new byte buffer for copying after read

                SysLib.rawread(target, newBuffer); // Read the target into the new buffer

                // Init ints to track pointer numbers
                int maxPtr = fileDesc.seekPtr % 512;
                int minPtr = 512 - maxPtr;

                // New iterator type ints
                int nextPos, nextPos2;
                nextPos = fsize(fileDesc) - fileDesc.seekPtr;
                nextPos2 = Math.min(Math.min(minPtr, curPos2), nextPos);

                // Perform a copy of the newBuffer into the input fileBuffer
                System.arraycopy(newBuffer, maxPtr, fileBuffer, curPos, nextPos2);

                fileDesc.seekPtr += nextPos2; // Move out seek pointer based on nextPos2
                curPos += nextPos2; // Move our current position up
                curPos2 -= nextPos2; // Move the second position down
            }

            return curPos; // On successful read, return the curPos int for read confirmation
        }
    }
}
