/**
 * Directory for use with File System. Directory starts with provided code, and
 * implements a single level directory structure for the File System to use.
 * There are helper methods and extra data members added onto the original code
 * to implement this directory
 * 
 * @author Alexander Auxier
 */

public class Directory
{
    private static int maxChar = 30;
    private static int maxSize = 4;
    private static char fDelimiter = '\u0000'; // used to delimit file names
    private static int err = -1; // Error
    private static int allClear = 0;
    private int fsizes[];
    private char fnames[][];
    private int maxBytes; // max amount of bytes needed to represent directory
    int maxInodes;

    /**
     * Directory constructor that inits with a new max number of iNodes.
     * @param new_maxInodes_value
     */
    public Directory(int maxInumber)
    {
        maxInodes = maxInumber;
        maxBytes = (maxInodes * maxSize) + (maxInodes * maxChar);
        initArrays(maxInodes);// After maxbytes calc, init new array
    }

    /**
     * This method takes a byte array to init the directory. Calls initArrays.
     */
    public int bytes2directory(byte data[])
    {
        clear(); // clear the current directory
        int currentIndex = 0; // counter used to keep track of index throughout various loops
        maxInodes = SysLib.bytes2int(data, currentIndex); // read in maxInodes (used for loops)
        currentIndex += maxSize; // bytes2int reads maxSize elements
        initArrays(maxInodes); // init array

        // Read in file size vals
        for(int i = 0; i < maxInodes; i++)
        {
            fsizes[i] = SysLib.bytes2int(data, currentIndex);
            currentIndex += maxSize;
        }

        // Read in files
        for(int j = 0; j < maxInodes; j++)
        {
            for(int k = 0; k < maxChar; k++)
            {
                // Use our delimiter for file names to check and break
                if((char) data[currentIndex] == fDelimiter)
                {
                    currentIndex++;
                    break;
                }
                else
                {
                    fnames[j][k] = (char) data[currentIndex];
                    currentIndex++;
                }
            }
        }

        return allClear;
    }

    /**
     * Formats directory into its equivalent byte
     */
    public byte[] directory2bytes()
    {
        byte dir[] = new byte[maxBytes];
        int currentIndex = 0;

        // First item in the byte is the number of iNodes
        SysLib.int2bytes(maxInodes, dir, currentIndex);
        currentIndex += maxSize; // int2bytes fills maxSize elements

        // Add all of the file sizes.
        for(int i = 0; i < maxInodes; i++)
        {
            SysLib.int2bytes(fsizes[i], dir, currentIndex);
            currentIndex += maxSize; // int2bytes fills maxSize elements
        }

        // Add all of the file names.
        for(int j = 0; j < maxInodes; j++)
        {
            for(int k = 0; k < maxChar; k++)
            {
                if(fnames[j][k] == fDelimiter)
                {
                    dir[currentIndex] = (byte) fDelimiter;
                    currentIndex++;
                    break;
                }
                dir[currentIndex] = (byte) fnames[j][k];
                currentIndex++;
            }
        }
        // We're all done!
        return dir;
    }

    /**
     * Receive an input string to generate new files with said input.
     */
    public short ialloc(String filename)
    {
        int inode = err; // initialize to error just in case we are full.

        // find the first space available.
        for(int i = 0; i < maxInodes; i++)
        {
            if(fsizes[i] == 0) // 0 fsize means free space
            {
                inode = i;
                break;
            }
        }

        // Set inode's size and name.
        if(inode != err)
        {
            fsizes[inode] = filename.length();
            filename.getChars(0, fsizes[inode], fnames[inode], 0);
        }

        return (short) inode;
    }

    /**
     * Remove files based on an input for a given iNode
     */
    public boolean ifree(short iNumber)
    {
        if(fsizes[iNumber] == 0 || iNumber <= 0 || iNumber >= maxInodes)
            return false;

        fsizes[iNumber] = 0; // Reset current fsize

        // Clear current file names
        for(int i = 0; i < maxChar; i++)
            fnames[iNumber][i] = fDelimiter;

        return true;
    }

    /**
     * Use an input name to search our iNodes and return the iNode that matches our
     * parameter. Return an error if this doesn't exist.
     */
    public short namei(String filename)
    {
        boolean found = false; // Init and assume false target iNode on filename
        
        if(filename.length() > maxChar)
            return (short) err;

        // convert string to char array for comparison
        char search[] = filename.toCharArray();
        int inode = err; // initialize to error just in case its not found

        for(int i = 0; i < maxInodes; i++)
        {
            if(fsizes[i] == search.length)
            {
                found = true; // Use a flag to break

                for(int j = 0; j < search.length; j++)
                {
                    if(search[j] == fnames[i][j])
                        continue;
                    else
                    {
                        found = false;
                        break;
                    }
                }

                // Check flag. If successful then break out of the loop.
                if(found)
                {
                    inode = i;
                    break;
                }
            }
        }

        return (short) inode;
    }

    /**
     * Clear the current directory.
     */
    public void clear()
    {
        // Clear fsizes
        for(int i = 0; i < maxInodes; i++)
            fsizes[i] = 0;

        // Clear fnames
        for(int j = 0; j < maxInodes; j++)
            for(int k = 0; k < maxChar; k++)
                fnames[j][k] = fDelimiter;
    }

    /**
     * Cycle through fnames and print to console
     */
    public void printFileNames()
    {
        for(int i = 0; i < maxInodes; i++)
        {
            for(int j = 0; j < maxChar; j++)
            {
                if(fnames[i][j] == fDelimiter)
                    break;

                System.out.print(fnames[i][j]);
            }

            System.out.println();
        }
    }

    /**
     * Helper method to init our directory related arrays.
     * Method is confined to maxSize
     */
    private void initArrays(int maxInumber)
    {
        fsizes = new int[maxInumber]; // Create our new array to modify

        // Populate the array with empty values
        for(int i = 0; i < maxInumber; i++)
            fsizes[i] = 0;

        // Init the fnames and init the first file as root
        fnames = new char[maxInumber][maxChar];
        String root = "/";
        fsizes[0] = root.length();
        root.getChars(0, fsizes[0], fnames[0], 0);
    }
}
