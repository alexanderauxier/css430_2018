/**
 * File Table Entry for use in File Table. The code for the file is provided by
 * the assignment page. A size getter had been added to the provided code, and
 * the iNode count is implemented in this file.
 * 
 * @author (Modified by) Alexander Auxier
 */

public class FileTableEntry
{
    // Each table entry should have
    public int seekPtr; // a file seek pointer
    public final Inode inode; // a reference to its inode
    public final short iNumber; // this inode number
    public int count; // # threads sharing this entry
    public final String mode; // "r", "w", "w+", or "a"

    public FileTableEntry(Inode i, short inumber, String m)
    {
        seekPtr = 0; // the seek pointer is set to the file top
        inode = i;
        iNumber = inumber;
        count = 1; // at least on thread is using this entry
        mode = m; // once access mode is set, it never changes
        if(mode.compareTo("a") == 0) // if mode is append,
            seekPtr = inode.curFsize; // seekPtr points to the end of file
    }

    /**
     * Getter for iNode count on a FTE. Used to avoid direct access (ie use this
     * method instead of calling the variables.) Variables a left public just in
     * case however.
     * 
     * @return iNode Count
     */
    public short getInodeCount()
    {
        return inode.count;
    }

    /**
     * Return the current file size in the table. -1 is invalid size return.
     * 
     * @return current file size in file table
     */
    public int getFileEntrySize()
    {
        if(inode.curFsize >= 0)
            return inode.curFsize;

        else
            return -1;
    }
}
