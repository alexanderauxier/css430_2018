/**
 * iNode for use with File System
 * 
 * @author Alexander Auxier
 */

public class Inode
{
    private final static int iNodeSize = 32; // fix to 32 bytes
    private final static int directSize = 11; // # direct pointers

    public int curFsize; // file size in bytes
    public short count; // # file-table entires pointing to this
    public short flag; // 0 = unused, 1 = used
    public short direct[] = new short[directSize]; // direct pointers
    public short indirect; // a indirect pointer

    /**
     * Default Constructor with our generic init values
     */
    Inode()
    {
        curFsize = 0;
        count = 0;
        flag = 1;

        for(int i = 0; i < directSize; i++)
            direct[i] = -1;

        indirect = -1;
    }

    /**
     * This constructor reads the corresponding disk block locates the corresponding
     * inode information int that block initializes a new inode with this
     * information
     */
    Inode(short iNumber)
    {
        int blockNumber = 1 + iNumber / 16; // Upto 16 iNodes store in a single block

        // read disk block ad store into data
        byte[] data = new byte[Disk.blockSize];
        SysLib.rawread(blockNumber, data);

        // determine length, count, offset and flag fron the given data
        int offset = ((iNumber % 16) * 32);
        curFsize = SysLib.bytes2int(data, offset);
        offset += 4;
        count = SysLib.bytes2short(data, offset);
        offset += 2;
        flag = SysLib.bytes2short(data, offset);
        offset += 2;

        // get direct pointers from data
        for(int i = 0; i < directSize; i++)
        {
            direct[i] = SysLib.bytes2short(data, offset);
            offset = offset + 2;
        }

        // also get indrect from end
        indirect = SysLib.bytes2short(data, offset);
    }

    /**
     * Before an inode is updated check the corresponding inode on disk read it form
     * the disk if the disk has been updated by another thread write back the
     * contents to disk immediately, including length, count, flag, direct[11],
     * indirect
     */
    @SuppressWarnings("static-access")
    int toDisk(short iNumber)
    {
        int blockNumber = (iNumber / 16) + 1;
        int relativeINumber = iNumber % 16;

        // read disk block and store into data
        byte[] data = new byte[Disk.blockSize];
        SysLib.rawread(blockNumber, data);

        // get length, count, flag into data
        int offset = 0;
        SysLib.int2bytes(this.curFsize, data, relativeINumber * this.iNodeSize + offset);

        offset += 4;
        SysLib.short2bytes(this.count, data, relativeINumber * this.iNodeSize + offset);

        offset += 2;
        SysLib.short2bytes(this.flag, data, relativeINumber * this.iNodeSize + offset);

        // copy direct values into data
        offset += 2;
        for(int i = 0; i < directSize; i++, offset += 2)
            SysLib.short2bytes(this.direct[i], data, relativeINumber * this.iNodeSize + offset);

        // copy indrect
        SysLib.short2bytes(this.indirect, data, relativeINumber * this.iNodeSize + offset);

        // write into data
        SysLib.rawwrite(blockNumber, data);

        return 0;
    }

    /**
     * Method for registering a target block for an inode Added for FileSystem write
     * function. Return values correspond to write case number values
     */
    public int registerTargetBlock(int currentPtr, short blockNum)
    {
        int newPtr = currentPtr / 512; // Set our new pointer from the passed in pointer

        if(newPtr < 11) // Check for valid pointer bounds
        {
            // Error Case 1: Invalid direct ptr @ newPtr
            if(direct[newPtr] >= 0)
                return -1; // Error recieved as -1

            // Error Case 2: Invalid newPtr and Invalid newPtr--
            if(newPtr > 0 && (direct[(newPtr - 1)] == -1))
                return -2; // Error recieved as -2

            direct[newPtr] = blockNum; // Set the pointer indexed as the blockNum

            return 0; // Return 0 for valid case
        }

        // Error Case 3: Indirect pointer is invalid
        if(indirect < 0)
            return -3; // Return -3 for error

        byte[] iBuffer = new byte[512]; // Set a new buffer for read
        SysLib.rawread(indirect, iBuffer); // Read with the indirect point into the buffer

        int nextPtr = newPtr - 11; // Get our next pointer

        // Check if our current buffer value is a valid short
        if(SysLib.bytes2short(iBuffer, nextPtr * 2) > 0)
        {
            // Send and error message via SysLib.cerr to indicate incorrect values
            SysLib.cerr("Current Index Block + Current indirect pointer: " + nextPtr + " Contents at Block = "
                    + SysLib.bytes2short(iBuffer, nextPtr * 2) + "\n");

            return -1; // Error will be marked -1
        }

        // Else, we have valid data
        SysLib.short2bytes(blockNum, iBuffer, nextPtr * 2); // Convert our short into our buffer
        SysLib.rawwrite(indirect, iBuffer); // Write the buffer based off the indirect pointer

        return 0; // Successful register on target blocknum
    }

    /**
     * Method for registering the next index block of an Inode based off a index as
     * a short. Used for FileSytem.java
     */
    public boolean registerIndexBlock(short newIndex)
    {
        // Loop over the direct pointers and check for invalid (-1)
        for(int i = 0; i < 11; i++)
            if(direct[i] == -1) // Catch invalid pointers
                return false; // Return false, unable to register

        if(indirect != -1) // Check for an invalid indirect pointer
            return false; // Return false, unable to register

        indirect = newIndex; // Reset our indirect pointer

        byte[] iBuffer = new byte[512]; // Create a new byter buffer for writting

        // Loop through the buffer and convert the short data into bytes
        for(int i = 0; i < 256; i++)
            SysLib.short2bytes((short) -1, iBuffer, i * 2);

        SysLib.rawwrite(newIndex, iBuffer); // Write the index into the buffer

        return true; // True on successful register
    }

    /**
     * Method for finding target block based of an input offset Added from
     * additional material PDF
     */
    public short findTargetBlock(int offset)
    {
        short retBlock; // Init variable used for holding return value
        int curPos = offset / 512; // Get the current position based off the input offset

        // If the curPos < 11, then we use this as our target block to return
        if(curPos < 11)
            return direct[curPos];

        // Before continuing, check if indirect pointer is valid
        if(indirect < 0)
            return -1; // Error on invalid pointer

        byte[] iBuffer = new byte[512]; // Byte buffer for return
        int nextPos = curPos - 11; // Next position to get target block

        SysLib.rawread(indirect, iBuffer); // Use indirect pointer to read into buffer

        retBlock = SysLib.bytes2short(iBuffer, nextPos * 2); // Set our return block

        return retBlock; // Return our targeted block
    }
}
