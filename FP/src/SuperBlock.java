/**
 * SuperBlock for the File System. Built off of initially provided code snippet.
 * 
 * @author Alexander Auxier
 */

public class SuperBlock
{
    private final static int DEFAULT_INODES = 64;
    private final static int INODES_PER_BLOCK = 16;
    private final static int DEFAULT_FREE_LIST = 5;
    public int totalBlocks;
    public int totalInodes;
    public int freeList;

    /**
     * C'tor for the superblock
     */
    public SuperBlock(int diskSize)
    {
        totalBlocks = diskSize;
        totalInodes = DEFAULT_INODES;
        freeList = DEFAULT_FREE_LIST;
    }

    /**
     * C'tor based on the iNodes and the disk size
     */
    public SuperBlock(int diskSize, int numInodes)
    {
        totalBlocks = diskSize;
        totalInodes = numInodes;

        // Start using free list to modify and setup for superblock

        if(numInodes % INODES_PER_BLOCK != 0)
            freeList = (numInodes / INODES_PER_BLOCK) + 2;

        else
            freeList = (numInodes / INODES_PER_BLOCK) + 1;
    }

    /**
     * Init on the byte array for format
     */
    public SuperBlock(byte byteArray[])
    {
        int offset = 0;

        totalBlocks = SysLib.bytes2int(byteArray, offset);
        offset += 4;

        totalInodes = SysLib.bytes2int(byteArray, offset);
        offset += 4;

        freeList = SysLib.bytes2int(byteArray, offset);
    }

    /**
     * Format helper function for FileSystem. Reformats SuperBlock. This separates
     * formatting the SuperBlock from the FileSytem code, and keeps it local to the
     * SuperBlock itself.
     */
    public void format(int maxFiles)
    {
        totalInodes = maxFiles; // Reset total number of iNodes

        Inode curNode; // Inode for formatting
        byte[] blockBuffer; // Byte buffer for formatting SuperBlock

        // Loop over max files for formatting
        for(int i = 0; i < maxFiles; i++)
        {
            curNode = new Inode(); // New iNode for each iteration

            curNode.flag = 0; // Reset current iNode flag to zero
            curNode.toDisk((short) i); // Check and updated disk contents
        }

        freeList = (2 + maxFiles * 32 / 512); // Modify freelist for byte format

        // Loop over the amount of blocks versus the freelist
        for(int curList = freeList; curList < totalBlocks; curList++)
        {
            blockBuffer = new byte[512]; // Init our buffer for reset

            // Init each point in the buffer to zero
            for(int j = 0; j < 512; j++)
                blockBuffer[j] = 0;

            // Convert our list and write the buffer
            SysLib.int2bytes(curList + 1, blockBuffer, 0);
            SysLib.rawwrite(curList, blockBuffer); // Use raw write to the disk
        }

        sync(); // Ensure contents are written back to the disk
    }

    public void sync()
    {
        byte[] blockBuffer = new byte[512]; // New buffer for writing to disk

        SysLib.int2bytes(totalBlocks, blockBuffer, 0); // Add totalBlock to buffer
        SysLib.int2bytes(totalInodes, blockBuffer, 4); // Add totalInodes to buffer
        SysLib.int2bytes(freeList, blockBuffer, 8); // Add freelist

        SysLib.rawwrite(0, blockBuffer); // Write contents to disk

        SysLib.cout("SuperBlock: Sync Complete");
    }

    /**
     * Default format Max number of files is assumed 64
     */
    public void format()
    {
        format(64);
    }

    /**
     * Write back totalBlocks, totalInodes and freeList to the Disk Added based on
     * additional material spec.
     */

    /**
     * Enqueues blocks to the end of the freeList Added based on additional material
     * spec
     */
    public boolean returnBlock(int currentBlock)
    {
        // Check for valid block num
        if(currentBlock >= 0)
        {
            byte[] blockBuffer = new byte[512]; // New byte buffer

            // Init buffer to zero
            for(int i = 0; i < 512; i++)
                blockBuffer[i] = 0;

            SysLib.int2bytes(freeList, blockBuffer, 0); // Convert and write freeList to blockBuffer
            SysLib.rawwrite(currentBlock, blockBuffer); // Write current block into buffer

            freeList = currentBlock; // Reset freeList to the currentBlock num

            return true; // Block successfully returned
        }

        return false; // Default return on incomplete returnBlock call
    }

    /**
     * Dequeues top block from freeList Added based on additional material spec
     */
    public int getFreeBlock()
    {
        // Free block to return will start with current freeList block value
        int retFreeBlock = freeList;

        /*
         * Check for valid block If valid begin dequeue process to get free block Once
         * written, we will return
         */
        if(retFreeBlock != -1) // Check for valid block
        {
            byte[] blockBuffer = new byte[512]; // Init new buffer

            // Read current block to buffer
            SysLib.rawread(retFreeBlock, blockBuffer);
            freeList = SysLib.bytes2int(blockBuffer, 0);

            // Write block for return
            SysLib.int2bytes(0, blockBuffer, 0);
            SysLib.rawwrite(retFreeBlock, blockBuffer);
        }

        return retFreeBlock; // Returned top block from list on successful dequeue
    }

    /**
     * Formats the bytes to a usable array
     */
    public byte[] toByteArray()
    {
        byte cBlock[] = new byte[12];
        int offset = 0;

        SysLib.int2bytes(totalBlocks, cBlock, offset);
        offset += 4;

        SysLib.int2bytes(totalInodes, cBlock, offset);
        offset += 4;

        SysLib.int2bytes(freeList, cBlock, offset);

        return cBlock;
    }
}
