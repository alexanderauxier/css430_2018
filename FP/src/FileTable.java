
/**
 * File Table implemented for use in the File System.
 * 
 * @author Alexander Auxier
 */

import java.util.Vector;

public class FileTable
{
    private Vector<FileTableEntry> curTable; // the actual entity of this file table
    private Directory curDir; // the root directory

    /**
     * Constructor to init the new File Table with our current directory
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public FileTable(Directory directory)
    {
        curTable = new Vector();
        curDir = directory;
    }

    /**
     * File allocation method for the FT
     * 
     * iNode: Unused = 0 Used = 1 Read = 2 Write = 3
     */
    public synchronized FileTableEntry falloc(String filename, String mode)
    {
        Inode temp;
        short iNodeNum;

        // if root, inode is unused (0)
        if(filename == "/")
            iNodeNum = 0;
        else
            iNodeNum = curDir.namei(filename); // else search for the file

        // if empty inode is found
        if(iNodeNum == -1)
        {

            // create the file if mode allows
            if(mode.equalsIgnoreCase("w") || mode.equalsIgnoreCase("w+") || mode.equalsIgnoreCase("a"))
            {
                iNodeNum = curDir.ialloc(filename);
                temp = new Inode();
                temp.flag = 2;
            }

            else if(mode.equalsIgnoreCase("r"))
            {
                // print error if read only
                SysLib.cerr("ThreadOS: Read only file, can't overwrite.\n");
                return null;
            }

            else
            {
                SysLib.cerr("ThreadOS: Problem writing file.\n");
                return null;
            }
        }

        else
        {
            temp = new Inode(iNodeNum);
            temp.flag = 1;
        }

        temp.count++;
        temp.toDisk(iNodeNum);

        // creating a new file table entry and adding onto the table
        FileTableEntry entry = new FileTableEntry(temp, iNodeNum, mode);
        this.curTable.add(entry);

        return entry;
    }

    /**
     * Return empty or not
     */
    public synchronized boolean fempty()
    {
        return curTable.isEmpty();
    }

    /**
     * Use this method to check if node(s) are free
     */
    public synchronized boolean ffree(FileTableEntry e)
    {
        if(!this.curTable.contains(e))
            return false;

        else
        {
            this.curTable.removeElement(e);
            e.inode.count--;
            e.inode.toDisk(e.iNumber);

            return true;
        }
    }
}