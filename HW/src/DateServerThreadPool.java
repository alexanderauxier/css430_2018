
/**
 * 
 */
//Example 3.21 from book

import java.net.*;
import java.io.*;

public class DateServerThreadPool implements Runnable
{
    private static Socket client = null;
    private static PrintWriter pout = null;
    
    /*public static void main(String[] args)
    {
        try
        {
            ServerSocket sock = new ServerSocket(6013);

             now listen for connections 
            while(true)
            {
                client = sock.accept();
                pout = new PrintWriter(client.getOutputStream(), true);

                 write the Date to the socket 
                pout.println(new java.util.Date().toString());

                 close the socket and resume 
                 listening for connections 
                client.close();
            }
        }

        catch(IOException ioe)
        {
            System.err.println(ioe);
        }
    }*/

    @Override
    public void run()
    {
        try
        {
            ServerSocket sock = new ServerSocket(6013);

            /* now listen for connections */
            while(true)
            {
                client = sock.accept();
                pout = new PrintWriter(client.getOutputStream(), true);

                /* write the Date to the socket */
                pout.println(new java.util.Date().toString());

                /* close the socket and resume */
                /* listening for connections */
                client.close();
            }
        }

        catch(IOException ioe)
        {
            System.err.println(ioe);
        }
        
    }
}
