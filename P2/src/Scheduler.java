/**
 * Program 2 Part 2 (MLFQ):
 *  This part will be left as the standard entry for the Scheduler. There will be an
 *      additional copy of this file as .part2 as backup.
 *  Helper methods are used to accomplish the MLFQ.
 *  MLFQ uses three queues, with three time quantum's. This will confine each queue to a
 *      the assignment specific time slice, and maintain thread execution.
 *  Code from part 1 carries over where applicable, and modified for MLFQ.
 *  Queues are processed based on the run method evoking the scheduler to run. Each 
 *      queue in our MLFQ uses its own helper method to execute, based on assignment
 *      spec timeslices. If something is not completed by our final queue, then we 
 *      restart it at the end of the final queue.
 *
 * @author Alexander Auxier
 * Date: 2018-4-23
 */

import java.util.*;

public class Scheduler extends Thread
{
    private Vector queue0; // TCB timeSlice /= 2
    private Vector queue1; // TCB timeSlice == timeSlice
    private Vector queue2; // TCB timeSlice *= 2
    private int timeSlice;

    private static final int DEFAULT_TIME_SLICE = 1000;

    // New data added to p161
    private boolean[] tids; // Indicate which ids have been used
    private static final int DEFAULT_MAX_THREADS = 10000;

    // A new feature added to p161
    // Allocate an ID array, each element indicating if that id has been used
    private int nextId = 0;

    private void initTid(int maxThreads)
    {
        tids = new boolean[maxThreads];
        for(int i = 0; i < maxThreads; i++)
            tids[i] = false;
    }

    // A new feature added to p161
    // Search an available thread ID and provide a new thread with this ID
    private int getNewTid()
    {
        for(int i = 0; i < tids.length; i++)
        {
            int tentative = (nextId + i) % tids.length;
            if(tids[tentative] == false)
            {
                tids[tentative] = true;
                nextId = (tentative + 1) % tids.length;
                return tentative;
            }
        }
        return -1;
    }

    // A new feature added to p161
    // Return the thread ID and set the corresponding tids element to be unused
    private boolean returnTid(int tid)
    {
        if(tid >= 0 && tid < tids.length && tids[tid] == true)
        {
            tids[tid] = false;
            return true;
        }
        return false;
    }

    // A new feature added to p161
    // Retrieve the current thread's TCB from the queue
    /*
     * Original method modified for part 2. Added synchronized sections for each
     * part of the MLFQ
     */
    public TCB getMyTcb()
    {
        Thread myThread = Thread.currentThread(); // Get my thread object
        synchronized(queue0)
        {
            for(int i = 0; i < queue0.size(); i++)
            {
                TCB tcb = (TCB) queue0.elementAt(i);
                Thread thread = tcb.getThread();
                if(thread == myThread) // if this is my TCB, return it
                    return tcb;
            }
        }

        // Below two synchronized blocks are identical to main block above
        synchronized(queue1)
        {
            for(int i = 0; i < queue1.size(); i++)
            {
                TCB tcb = (TCB) queue1.elementAt(i);
                Thread thread = tcb.getThread();
                if(thread == myThread) // if this is my TCB, return it
                    return tcb;
            }
        }

        synchronized(queue2)
        {
            for(int i = 0; i < queue2.size(); i++)
            {
                TCB tcb = (TCB) queue2.elementAt(i);
                Thread thread = tcb.getThread();
                if(thread == myThread) // if this is my TCB, return it
                    return tcb;
            }
        }
        return null;
    }

    // A new feature added to p161
    // Return the maximal number of threads to be spawned in the system
    public int getMaxThreads()
    {
        return tids.length;
    }

    public Scheduler()
    {
        timeSlice = DEFAULT_TIME_SLICE;
        queue0 = new Vector();
        queue1 = new Vector();
        queue2 = new Vector();
        initTid(DEFAULT_MAX_THREADS);
    }

    public Scheduler(int quantum)
    {
        timeSlice = quantum;
        queue0 = new Vector();
        queue1 = new Vector();
        queue2 = new Vector();
        initTid(DEFAULT_MAX_THREADS);
    }

    // A new feature added to p161
    // A constructor to receive the max number of threads to be spawned
    /**
     * Constructor for time quantums and threads for the scheduler. Has been
     * modified for MLFQ
     */
    public Scheduler(int quantum, int maxThreads)
    {
        timeSlice = quantum;
        queue0 = new Vector();
        queue1 = new Vector();
        queue2 = new Vector();
        initTid(maxThreads);
    }

    private void schedulerSleep()
    {
        try
        {
            Thread.sleep(timeSlice);
        }
        catch(InterruptedException e)
        {
        }
    }

    // A modified addThread of p161 example
    public TCB addThread(Thread t)
    {
        t.setPriority(2);
        TCB parentTcb = getMyTcb(); // get my TCB and find my TID
        int pid = (parentTcb != null) ? parentTcb.getTid() : -1;
        int tid = getNewTid(); // get a new TID
        if(tid == -1)
            return null;
        TCB tcb = new TCB(t, tid, pid); // create a new TCB

        // the following if and for statements are for file system.
        if(parentTcb != null)
        {
            for(int i = 0; i < 32; i++)
            {
                tcb.ftEnt[i] = parentTcb.ftEnt[i];
                // JFM added 2012-12-01
                // increment the count for any file table entries inherited from parent
                if(tcb.ftEnt[i] != null)
                    tcb.ftEnt[i].count++;
            }
        }

        queue0.add(tcb);
        return tcb;
    }

    // A new feature added to p161
    // Removing the TCB of a terminating thread
    public boolean deleteThread()
    {
        TCB tcb = getMyTcb();
        if(tcb == null)
            return false;
        else
        {
            // JFM added 2012-12-01
            // if any file table entries are still open, decrement their count
            for(int i = 3; i < 32; i++)
                if(tcb.ftEnt[i] != null)
                    // JFM changed 2012-12-13
                    // close any open file descriptors rather than decrement the counts
                    // to ensure that system-wide file table entries are removed
                    // when no longer needed
                    SysLib.close(i);
            return tcb.setTerminated();
        }
    }

    public void sleepThread(int milliseconds)
    {
        try
        {
            sleep(milliseconds);
        }
        catch(InterruptedException e)
        {
            // No error messages here
        }
    }

    // A modified run of p161
    @SuppressWarnings("deprecation")
    /*
     * Modified from original part 1 section. Removed old control block, and added a
     * new one with use of the helper methods Helper methods run through each queue
     * for the assignment spec
     */
    public void run()
    {
        Thread current = null;

        // this.setPriority(6); //REMOVED FOR part 1 testing

        while(true)
        {
            try
            {
                // Check for empty queues
                if(areQueuesEmpty())
                    continue;

                // Start with queue0
                if(moreContent(queue0))
                {
                    if(executeQ0(current))
                        continue;

                    continue;
                }

                // Move to queue1
                if(isQueueEmpty(queue0) && moreContent(queue1))
                {
                    if(executeQ1(current))
                        continue;

                    continue;
                }

                // Finally move to queue2
                if(isQueueEmpty(queue0) && isQueueEmpty(queue1) && moreContent(queue2))
                {
                    if(executeQ2(current))
                        continue;

                    continue;
                }
            }
            catch(NullPointerException e3)
            {
            }
            ; //My Eclipse formatter keeps doing this and idk why I can't change just this behavior
        }
    }

    /**
     * Use this to check if the queues are empty
     * 
     * @return true == all queues are empty
     */
    private boolean areQueuesEmpty()
    {
        return(queue0.isEmpty() && queue1.isEmpty() && queue2.isEmpty());
    }

    /**
     * Method used for checking if an individual queue is empty.
     * 
     * @param current queue
     * @return isEmpty?
     */
    private boolean isQueueEmpty(Vector curQ)
    {
        return(curQ.size() == 0);
    }

    /**
     * Checker for ensuring content is in a queue Used to continued scheduling run
     * method. Use size to double check not empty
     * 
     * @param curQ
     * @return current size
     */
    private boolean moreContent(Vector curQ)
    {
        return(curQ.size() > 0);
    }

    /**
     * Method for processing the first queue, which is queue0 in the MLFQ setup
     * 
     * @param curThread
     * @return
     */
    private boolean executeQ0(Thread curThread)
    {
        TCB currentTCB = (TCB) queue0.firstElement();

        if(deadThreadCheck(currentTCB, queue0))
            return true;

        curThread = currentTCB.getThread();

        restartThread(curThread);
        sleepThread(timeSlice / 2);

        // Finish
        finalQueueExe(queue0, queue1, curThread, currentTCB);

        return false;
    }

    /**
     * Method for processing queue1 based on the MLFQ setup
     * 
     * @param curThread
     * @return
     */
    private boolean executeQ1(Thread curThread)
    {
        TCB currentTCB = (TCB) queue1.firstElement();

        if(deadThreadCheck(currentTCB, queue1))
            return true;

        curThread = currentTCB.getThread();
        restartThread(curThread);

        sleepThread(timeSlice / 2);

        if(moreContent(queue0))
            addedNewTCB(curThread);

        sleepThread(timeSlice / 2);

        finalQueueExe(queue1, queue2, curThread, currentTCB);

        return false;
    }

    /**
     * Method for processing queue2
     * 
     * @param curThread
     * @return
     */
    private boolean executeQ2(Thread curThread)
    {
        TCB currentTCB = (TCB) queue2.firstElement();

        if(deadThreadCheck(currentTCB, queue2))
            return true;

        curThread = currentTCB.getThread();
        restartThread(curThread);

        sleepThread(timeSlice / 2);

        if(moreContent(queue0) || moreContent(queue1))
            addedNewTCB(curThread);

        sleepThread(timeSlice / 2);
        sleepThread(timeSlice);

        finalQueueExe(queue2, queue2, curThread, currentTCB);
        return false;
    }

    /**
     * This method is for the final process of each queue processing method
     * 
     * @param curQ
     * @param nextQ
     * @param curThread
     * @param curTCB
     */
    private void finalQueueExe(Vector curQ, Vector nextQ, Thread curThread, TCB curTCB)
    {
        synchronized(curQ)
        {
            if(curThread != null && curThread.isAlive())
            {
                curThread.suspend(); // Suspend thread

                // Move from one queue to the other
                curQ.remove(curTCB);
                nextQ.addElement(curTCB);
            }
        }
    }

    /**
     * This method is to extend each queue to handle a new TCB added to them
     */
    private void addedNewTCB(Thread curThread)
    {
        if(curThread != null && curThread.isAlive())
        {
            curThread.suspend(); // Suspend the thread

            Thread additionalProcess = null;

            executeQ0(additionalProcess); // Add to queue0

            curThread.resume();
        }
    }

    /**
     * Private method for checking for dead threads. Takes in a TCB and the current
     * queue
     * 
     * @param curTCB
     * @param curQ
     * @return
     */
    private boolean deadThreadCheck(TCB curTCB, Vector curQ)
    {
        if(curTCB.getTerminated() == true)
        {
            curQ.remove(curTCB);
            returnTid(curTCB.getTid());

            return true;
        }

        return false;
    }

    /**
     * This method is for resuming/restarting the current thread
     * 
     * @param curThread
     */
    private void restartThread(Thread curThread)
    {
        if(curThread != null)
        {
            if(curThread.isAlive())
                curThread.resume();

            else
                curThread.start();
        }
    }

}
