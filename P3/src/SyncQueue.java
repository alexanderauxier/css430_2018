/**
 * Program 3 SyncQueue
 * 
 * SyncQueue utilizes QueueNode, which allows for an abstracted vector in the
 * form of an array.
 * 
 * @author Alexander Auxier 
 * Date: 2018-5-7
 */

public class SyncQueue
{
    private QueueNode[] syncQueue = null; // Array of QueueNodes used for program 3

    private final int MAX_CONDS = 10; // Speced default for max conditions

    /**
     * Basic constructor uses our generic final int for init. This sets the stage
     * for our max size of 10
     */
    public SyncQueue()
    {
        initSyncQueue(MAX_CONDS);
    }

    /**
     * SyncQueue constructor for generating a new queue of a max number of
     * conditions. This will differ from final int value previously defined.
     * 
     * @param condMax
     */
    public SyncQueue(int condMax)
    {
        initSyncQueue(condMax);
    }

    /**
     * Universal method for initializing the sync queue Allows for expansion on
     * SyncQueue, so we can re-factor later and continue to use this method to
     * generate the SyncQueue
     * 
     * @param input size
     */
    private void initSyncQueue(int initSize)
    {
        // Cannot init queue if we have a queue
        if(syncQueue != null)
            return;

        syncQueue = new QueueNode[initSize]; // use the input for the size

        // Loop across the size, generate QueueNodes at each index
        for(int i = 0; i < syncQueue.length; i++)
            syncQueue[i] = new QueueNode(); // Index must have a QueueNode
    }

    /**
     * This method enqueues and sleeps threads at the index/condition value
     */
    public int enqueueAndSleep(int condition)
    {
        if(condition >= 0 && condition < syncQueue.length) // Successful queue entry
            return syncQueue[condition].sleep(); // Sleep thread

        return 0; // Generic output to keep code functioning
    }

    /**
     * This method assumes the current Thread ID is 0, and will call the second
     * dequeueAndWakeup method.
     */
    public void dequeueAndWakeup(int condition)
    {
        dequeueAndWakeup(condition, 0); // Jump to perform dequeue
    }

    /**
     * This method takes in a condition integer, and a Thread ID and wakes the
     * thread in the queue at the condition parameter.
     * 
     * If there is an error on the dequeue we do nothing
     */
    public void dequeueAndWakeup(int condition, int tid)
    {
        if(condition >= 0 && condition < syncQueue.length) // Dequeue on successful conditions
            syncQueue[condition].wakeup(tid); // Wake thread
    }
}
