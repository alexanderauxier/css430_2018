
/**
 * QueueNode Data Structure for SyncQueue
 * 
 * QueueNode is a Vector, with additional methods. This reduces error making a
 * new data structure from scratch, and allows for reuse of exiting function
 * calls. QueueNode will use a raw type Vector since our vector will hold
 * objects later.
 * 
 * @author Alexander Auxier 
 * Date: 2018-5-7
 */

import java.util.Vector;

public class QueueNode
{
    @SuppressWarnings("rawtypes")
    private Vector qNode = null; // Vector should be initialized to null

    /**
     * Main constructor for QueueNode QueueNodes should be made without calling
     * parameterized constructors. Vector will be cleared when created (safety step)
     */
    @SuppressWarnings("rawtypes")
    public QueueNode()
    {
        qNode = new Vector();

        // Our queue node must be empty
        if(!qNode.isEmpty())
            qNode.clear();
    }

    /**
     * Sleep method for sleeping threads in QueueNode DS
     */
    public synchronized int sleep()
    {
        if(qNode.isEmpty()) // Check for empty QueueNode
        {
            try
            {
                // Here we will wait so we can sleep
                wait(); // Anticipated data
            }

            // Catch local interrupt error
            catch(InterruptedException localInteruptedException)
            {
                // Send -1 as a simplified error message.
                return -1;
            }
        }

        Integer sleepInt = (Integer) qNode.remove(0); // Cast and sleep at 0

        return sleepInt.intValue(); // return must be an int for this method to function
    }

    /**
     * Wake-up method for waking threads.
     * This also adds to the QueueNode DS
     */
    @SuppressWarnings("unchecked")
    public synchronized void wakeup(int wakeInt)
    {
        qNode.add(new Integer(wakeInt)); // Add data

        notify(); // Must notify other threads
    }
}
