
/**
 * Program 4 Cache: The goal of this class is to provide a Cache for the Thread
 * OS Kernel to use. Enables the Kernel to use the following calls in its
 * interrupt: boot, cread cwrite, csync, cflush.
 * 
 * Date: 2018-5-20
 * @author Alexander Auxier
 */

import java.util.Vector;

public class Cache
{
    private int cacheBlockSize = 0; // Store the current block size
    private int numCacheBlocks = 0; // Tracks current number of cache blocks/cachePage size
    private int currentVictim = -1; // Track next victim position

    // Vector for cache pages. Used with internal entry class
    @SuppressWarnings("rawtypes")
    private Vector cachePages = null;
    private Cache.CacheEntry[] cachePageTable = null; // Page table for the current cache

    /**
     * This class is used to create a specific entry data for each cache page.
     * Private variables here track the reference bit, dirty bit, and the frame
     * number in the block.
     */
    private class CacheEntry
    {
        /*
         * Variables that define an entry into a cache page we'll init our bits to 0
         * (false), and our block from to -1 until we define it later on for each entry.
         */
        private int currentBlockFrame = -1;
        private boolean referenceBit = false;
        private boolean dirtyBit = false;

        public CacheEntry()
        {
            // empty constructor that only exits for class level variables
        }
    }

    /**
     * Constructor for a new cache. Takes in specifications for number of cache
     * blocks, and block size byte data on memory.
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public Cache(int newBlockSize, int newNumCacheBlocks)
    {
        // Check for memory or init problems with pre-initialized data
        if(cacheBlockSize != 0 && numCacheBlocks != 0)
        {
            SysLib.cerr("CANNOT CREATE NEW CACHE");
            return;
        }

        // Initialize current private integers
        cacheBlockSize = newBlockSize;
        numCacheBlocks = newNumCacheBlocks;

        cachePages = new Vector(); // Init cachePages vector to add to

        // Loop through cache pages and add new byte array elements
        for(int i = 0; i < newNumCacheBlocks; i++)
        {
            byte[] curByteArray = new byte[newNumCacheBlocks];
            cachePages.addElement(curByteArray);
        }

        currentVictim = newNumCacheBlocks - 1; // Victim will be set to end slot
        cachePageTable = new Cache.CacheEntry[newBlockSize]; // Init new page table for cache

        // Loop through cache page table and add entries
        for(int i = 0; i < newNumCacheBlocks; i++)
            cachePageTable[i] = new Cache.CacheEntry();
    }

    /**
     * Method for sync and maintaining clean cache block copies.
     */
    public synchronized void sync()
    {
        // Loop through by size
        for(int i = 0; i < numCacheBlocks; i++)
        {
            rawWriteBack(i); // Use rawWriteBack to write back data
        }

        SysLib.sync(); // Invoke SysLib sync here
    }

    /**
     * Method for invalidating all cached blocks. Used when shutting down ThreadOS.
     */
    public synchronized void flush()
    {
        // Loop through the current size of the cache for the flush
        for(int i = 0; i < numCacheBlocks; i++)
        {
            rawWriteBack(i); // Write back data at current index
            cachePageTable[i].referenceBit = false; // Reset ref bit
            cachePageTable[i].currentBlockFrame = -1; // Reset current block frame
        }

        SysLib.sync(); // Invoke SysLib sync
    }

    /**
     * Reads the buffer based on the passed in blockID, if this is in the cache. If
     * not, read from disk block.
     * 
     * @return true = read, false = error
     */
    public synchronized boolean read(int blockID, byte[] buffer)
    {
        if(blockID < 0) // Check for invalid blockIDs
        {
            SysLib.cerr("CANNOT PERFORM CREAD @" + blockID + "\n"); // Invalid ID error
            return false;
        }

        int index; // Init loop iterator for later use
        byte[] currentByte; // Init byte data array for use in and out of loop

        /*
         * Loop through Cache and find the correct block ID. Return true if found
         */
        for(index = 0; index < numCacheBlocks; index++)
        {
            if(cachePageTable[index].currentBlockFrame == blockID)
            {
                currentByte = (byte[]) cachePages.elementAt(index); // Get byte data
                System.arraycopy(currentByte, 0, buffer, 0, cacheBlockSize); // Copy array here
                cachePageTable[index].referenceBit = true; // Set the reference bit

                return true; // Break true here
            }
        }

        index = getNextFreePage(); // Get next page frame

        if(index == -1) // If -1 get next victim instead
            index = getNextVictim();

        rawWriteBack(index); // Write back at the current index
        SysLib.rawread(blockID, buffer); // Raw Read with the blockID and buffer

        currentByte = new byte[cacheBlockSize]; // Byte array for copy
        System.arraycopy(currentByte, 0, buffer, 0, cacheBlockSize); // Copy data
        cachePageTable[index].referenceBit = true; // Set reference bit after copy

        return true;
    }

    /**
     * Writes the buffer contents to a specified cache block(blockID), if its in the
     * cache. If not, write to free cache block.
     * 
     * @param blockID
     * @param buffer
     * @return false for errors, true for writes.
     */
    @SuppressWarnings("unchecked")
    public synchronized boolean write(int blockID, byte[] buffer)
    {
        if(blockID < 0) // Check for invalid blockIDs
        {
            SysLib.cerr("CANNOT PERFORM CREAD @" + blockID + "\n"); // Invalid ID error
            return false;
        }

        int index; // Init index for use
        byte[] currentByte; // Init byte data array

        /*
         * Loop through the size of the page table If the blockID is found, copy data
         */
        for(index = 0; index < numCacheBlocks; index++)
        {
            if(cachePageTable[index].currentBlockFrame == blockID)
            {
                currentByte = new byte[cacheBlockSize]; // Set data array
                System.arraycopy(buffer, 0, currentByte, 0, cacheBlockSize); // Copy the buffer
                cachePages.set(index, currentByte); // Set our data at the current index

                // Set cache page table bits to true here
                cachePageTable[index].referenceBit = true;
                cachePageTable[index].dirtyBit = true;

                return true;
            }
        }

        index = getNextFreePage(); // Set index to next page frame

        if(getNextFreePage() == -1) // Check the next frame
            index = getNextVictim(); // If invalid use next victim

        rawWriteBack(index); // Write back at the current index value

        currentByte = new byte[cacheBlockSize]; // Set data array
        System.arraycopy(buffer, 0, currentByte, 0, cacheBlockSize); // Copy our buffer to the data array

        cachePages.set(index, currentByte); // Set the data at the previously set index

        // Set cache page table values for the write
        cachePageTable[index].currentBlockFrame = blockID;
        cachePageTable[index].referenceBit = true;
        cachePageTable[index].dirtyBit = true;

        return true;
    }

    /**
     * Helper method to write back all dirty blocks to the Disk Uses SysLib.rawwrite
     * and takes in the current index to write at.
     * 
     * @param writeIndex
     */
    private void rawWriteBack(int writeIndex)
    {
        /*
         * Check the current block frame and dirty bit for valid write parameters Exit
         * if invalid entry point
         */
        if(cachePageTable[writeIndex].currentBlockFrame != -1 && cachePageTable[writeIndex].dirtyBit == true)
        {
            byte[] currentByte = (byte[]) cachePages.elementAt(writeIndex); // Get byte data
            SysLib.rawwrite(cachePageTable[writeIndex].currentBlockFrame, currentByte); // Write data
            cachePageTable[writeIndex].dirtyBit = false; // Reset the dirty bit
        }

    }

    /**
     * Getter for finding the next free page frame.
     * 
     * @return Next page free
     */
    private int getNextFreePage()
    {
        int nextPage = -1; // Init here for error return

        for(int i = 0; i < numCacheBlocks; i++) // Loop over size of cache page table
        {
            if(cachePageTable[i].currentBlockFrame == -1) // Look for -1 to set
            {
                nextPage = i; // Set next page to i if true
                break; // Exit loop
            }
        }

        return nextPage; // Return the next page value
    }

    /**
     * Getter for obtaining the next victim
     * 
     * @return next victim
     */
    private int getNextVictim()
    {
        int retVictim = -1;

        // Loop until you hit the return
        while(true)
        {
            currentVictim = (currentVictim + 1) % numCacheBlocks; // Change current victim

            // Check if current victim's reference bit
            if(!cachePageTable[currentVictim].referenceBit)
            {
                retVictim = currentVictim; // Set for return
                return retVictim; // Return with the victim
            }

            cachePageTable[currentVictim].referenceBit = false; // Reset current reference bit for victim
        }
    }
}
