
/**
 * Test 4 for used in Program 4 Testing Scenarios Uses numbers 1-5 for the
 * following cache accesses: 1 = Random, 2 = Localized, 3 = Mixed, 4 =
 * Adversary, 5 = Test Case to run tests 1 - 4 back to back.
 * 
 * This test program will break, meaning it hangs in ThreadOS after a test case
 * is completed WITH cache enabled. You will have to restart it each time.
 * Please use test 5 for repeated testing to avoid this.
 * 
 * Date: 2018-5-20
 * @author Alexander Auxier
 */

import java.util.*;

public class Test4 extends Thread
{
    // TODO: Fix program hang...

    private boolean enabled;

    private int currentTest; // Track current test number from input

    // Time Longs
    private long begin; // Start time
    private long finish; // End Time

    // Byte Data Arrays
    private byte[] writeBuffer;
    private byte[] readBuffer;

    // Random for testing. Name unrelated
    private Random randyTheRando;

    private final static int MAX_ARRAY_SIZE = 200; // Cap at 200

    public void run()
    {
        SysLib.cout("Test 4 Begin\n");

        begin = new Date().getTime();

        SysLib.flush(); // Sys.flush to clear cache.

        switch(currentTest)
        {
            case 1:
                randomAccess();
                finish = new Date().getTime();

                getPerformance("random accesses");
                break;
            case 2:
                localizedAccess();
                finish = new Date().getTime();

                getPerformance("localized accesses");
                break;
            case 3:
                mixedAccess();
                finish = new Date().getTime();

                getPerformance("mixed accesses");
                break;
            case 4:
                adversaryAccess();
                finish = new Date().getTime();

                getPerformance("adversary accesses");
                break;
            case 5:
                randomAccess();
                finish = new Date().getTime();
                getPerformance("random accesses");

                begin = new Date().getTime();
                localizedAccess();

                finish = new Date().getTime();
                getPerformance("localized accesses");

                begin = new Date().getTime();
                mixedAccess();

                finish = new Date().getTime();
                getPerformance("mixed accesses");

                begin = new Date().getTime();
                adversaryAccess();

                finish = new Date().getTime();
                getPerformance("adversary accesses");
        }

        SysLib.cout("Test4 Complete\n");
        SysLib.exit();
    }

    public Test4(String[] inputAoStr)
    {
        enabled = (inputAoStr[0].equals("enabled"));

        currentTest = Integer.parseInt(inputAoStr[1]);

        writeBuffer = new byte[512];
        readBuffer = new byte[512];

        randyTheRando = new Random();
    }

    private void randomAccess()
    {
        int[] intArray = new int[MAX_ARRAY_SIZE];

        for(int i = 0; i < 200; i++)
            intArray[i] = Math.abs(randyTheRando.nextInt() % 512);

        for(int i = 0; i < 200; i++)
        {
            for(int j = 0; j < 512; j++)
                writeBuffer[j] = ((byte) j);

            write(intArray[i], writeBuffer);
        }

        for(int i = 0; i < 200; i++)
        {
            read(intArray[i], readBuffer);

            for(int j = 0; j < 512; j++)
                if(this.readBuffer[j] != writeBuffer[j])
                    SysLib.exit();
        }
    }

    private void localizedAccess()
    {
        for(int i = 0; i < 20; i++)
        {
            for(int j = 0; j < 512; j++)
                writeBuffer[j] = ((byte) (i + j));

            for(int j = 0; j < 1000; j += 100)
                write(j, writeBuffer);

            for(int j = 0; j < 1000; j += 100)
            {
                read(j, readBuffer);

                for(int k = 0; k < 512; k++)
                    if(readBuffer[k] != writeBuffer[k])
                        SysLib.exit();
            }
        }
    }

    private void mixedAccess()
    {
        int[] intArray = new int[MAX_ARRAY_SIZE];

        for(int i = 0; i < 200; i++)
        {
            if(Math.abs(randyTheRando.nextInt() % 10) > 8)
                intArray[i] = Math.abs(randyTheRando.nextInt() % 512);

            else
                intArray[i] = Math.abs(randyTheRando.nextInt() % 10);
        }

        for(int i = 0; i < 200; i++)
        {
            for(int j = 0; j < 512; j++)
                writeBuffer[j] = ((byte) j);

            write(intArray[i], writeBuffer);
        }

        for(int i = 0; i < 200; i++)
        {
            read(intArray[i], readBuffer);

            for(int j = 0; j < 512; j++)
                if(readBuffer[j] != writeBuffer[j])
                    SysLib.exit();
        }
    }

    private void adversaryAccess()
    {
        for(int i = 0; i < 20; i++)
        {
            for(int j = 0; j < 512; j++)
                writeBuffer[j] = ((byte) j);

            for(int j = 0; j < 10; j++)
                write(i * 10 + j, writeBuffer);
        }

        for(int i = 0; i < 20; i++)
        {
            for(int j = 0; j < 10; j++)
            {
                read(i * 10 + j, readBuffer);

                for(int k = 0; k < 512; k++)
                    if(readBuffer[k] != writeBuffer[k])
                        SysLib.exit();
            }
        }
    }

    /**
     * Method provides results output
     */
    private void getPerformance(String inputString)
    {
        if(enabled == true)
            SysLib.cout("Results of " + inputString + "[cache enabled] = " + (finish - begin) + "\n");

        else
            SysLib.cout("Results of " + inputString + "[cache disabled] =  " + (finish - begin) + "\n");
    }

    /**
     * Helper method for cache enabled, or disabled reads
     */
    private void read(int readIndex, byte[] buffer)
    {
        if(enabled == true)
            SysLib.cread(readIndex, buffer);

        else
            SysLib.rawread(readIndex, buffer);
    }

    /**
     * Helper method for cache enabled, or disabled reads
     */
    private void write(int writeIndex, byte[] buffer)
    {
        if(enabled == true)
            SysLib.cwrite(writeIndex, buffer);

        else
            SysLib.rawwrite(writeIndex, buffer);
    }
}
