//============================================================================
// Name        : processes.cpp
// Author      : Alexander Auxier
// Description : Processes source code for CSS430 Spring 2018. Program provides the
//				 functionality of running "$ ps -A | grep argv[1] | wc -l " 
//               No output is shown for 0 return value on grep and argument
//============================================================================

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
using namespace std;

///---MainFunction---///
/**
 * This function is the entry point, and execution code for process finding
 * shell statement. Argv[1] is the main arguement used for variable pass in,
 * while argc is present for error checking on the input variable count.
 * All system calls needed to implenet the functionality of grep are used through 
 * various if checks to hhen determine which processes we are dealing with, and 
 * which direction we need to move next to hit our end result.
 */
int main(int argc, char* argv[1])
{
	//Init our variables to start our grep journey
	int pipeFD1[2]; //File Descriptor array 1
	int pipeFD2[2]; //File Descriptor array 2
	int pipeSize; //Integer value for checking pipes

	//Testing if for argc variable
	if(argc < 2) //Check for correct arguments
		cerr << "Incorrect Arg Count" << endl;

	//Error checks for creating filed descriptor p
	if((pipeSize = pipe(pipeFD1)) < 0)
		cerr << "Error on pipe 1 FD" << endl;

	if((!(pipeSize = pipe(pipeFD2))) < 0)
		cerr << "Error on pipe 2 FD" << endl;

	pid_t curPID = fork(); //Obtain new child processID from current parent process

    /* Here we begin to check the current PID to determine how we work our process and 
     * execute based on the input parameter for grep.
     */
	if((curPID == 0))
	{
		//Child process fork
		curPID = fork(); //Obtain next child process from current child process

		if((curPID == 0))
		{
			//GrandChild Process Fork
			curPID = fork();

			if((curPID == 0))
			{
				//Great GrandChild Process fork
				close(pipeFD1[0]); //Close pipe 1 read
				dup2(pipeFD1[1], 1); //Write pipe 1 to console
                
				execlp("/bin/ps", "ps", "-A", NULL); //Out-statement
			}

			//Else, not working from grandchild process
			else
			{
				close(pipeFD1[1]); //Close pipe 1 writing
				dup2(pipeFD1[0], 0); //Read cin to pipe 1

				close(pipeFD2[0]); //Close read from pipe 2
				dup2(pipeFD2[1], 1); //Write pipe 2 to console out

				execlp("/bin/grep", "grep", argv[1], NULL); //Out-statment
			}
		}

		else
		{
			//Close pipe 1 so child processes will terminate
			close(pipeFD1[0]);
			close(pipeFD1[1]);

			close(pipeFD2[1]); //Close write on pipe 2
			dup2(pipeFD2[0], 0); //Read console in to pipe 2

			execlp("/bin/wc", "wc", "-l", NULL); //Console statement
		}
	}

	else
	{
		//This for loop closes pipes so the child processes can terminate
		for(int i = pipeFD1[0]; i <= pipeFD2[1]; i++)
			close(i);

		wait(NULL); //Wait to terminate
	}
    
    return 0; //If we reach here just exit with zero
}
