/**
 * Shell for CSS 430 Programming Assignment 1
 * The goal of this shell is to generate a basic shell usable on ThreadOS.
 * The shell can interpret single commands, and multiple commands that are
 * 	delimited with & and ; on input.
 *
 * Date 2018-4-10
 * @author Alexander Auxier
 */

import java.util.*;

public class Shell extends Thread
{
	private String cmdLine; //String for holding command line input
	private Object inputObj = null; //Initialize a new object variable to use
	
	/**
	 * Null Constructor
	 */
	public Shell()
	{
		cmdLine = ""; // Null constructor initializes to empty string
	}
	
	/**
	 * Loop for shell logic.
	 * We make use of the helper function getCommands to interpret the input commands on the shell.
	 */
	public void run()
	{
		for(int outerItr = 1;; outerItr++) //Main program loop and outside loop iterator
		{
			cmdLine = ""; //Init cmdLine to avoid potential empty string
			
			/* This do-while loop will cast the generic object as a string buffer.
			 * We're going to do this to avoid type casting issues, or object date
			 * 	dependency issues when we take in new objects through the shell.
			 * Our outer most loop iterator will be used to track the number of 
			 * 	executions for the shell here
			 */
			do
			{
				inputObj = new StringBuffer(); //Use newObj as a StringBuffer for the shell
				
				//Use Syslib to send console calls
				SysLib.cerr("SHELL[" + outerItr + "]% ");
				SysLib.cin((StringBuffer) inputObj); //Read in new objects as StringBuffers with cin.
				
				cmdLine = ((StringBuffer) inputObj).toString(); //Obtain strings from string buffer
			}while (cmdLine.length() == 0); //Execute once, then for each subsequent input
			
			inputObj = SysLib.stringToArgs(cmdLine); //Set the input as the commmand line
			String[] cmdStrs = (String[]) inputObj; //Cast input object into an array of commands strings
			
			int currentCount = 0; //Additional counter for loop
			
			for(int insideItr = 0; insideItr < cmdStrs.length; insideItr++) //Command loop with inner most iterator
			{
				//Check for delimiters in the input command strings
				if((cmdStrs[insideItr].equals(";")) || (cmdStrs[insideItr].equals("&")) || (insideItr == cmdStrs.length - 1))
				{
					//Generate string array of commands for shell to execute/read
					String[] inputCmds = getCommands((String[]) cmdStrs, currentCount, insideItr == cmdStrs.length - 1 ? insideItr + 1 : insideItr);
					
					//Error catch for NULL strings
					if(inputCmds != null)
					{
						SysLib.cerr(inputCmds[0] + "\n"); //Shell will continue input on next line after null
						
						//Exit on command
						if(inputCmds[0].equals("Exit"))
						{
							SysLib.exit(); //Use the SysLib exit call to exit the shell
							return;
						}
						
						int m = SysLib.exec(inputCmds); //Position for exec command
						
						//While loop to iterate m counter. Iterate as much as possible and make SysLib calls to find commands
						while ((m != -1) && (!cmdStrs[insideItr].equals("&")) && (SysLib.join() != m)){} //No loop contents
					}
					
					currentCount = insideItr + 1; //Change the current count to be incremented on the inside iterator.
				}
			}
		}
	}
	
	/**
	 * This method takes in an array of strings with commands/files or whatever the Shell needs to execute, and
	 * 	returns them into a string array for the shell to interpret and then execute upon. 
	 * This only supports the delimiters of & and ;
	 * 
	 * @param inputCommandsc
	 * @param position integer 1
	 * @param position integer 2
	 * @return input commands for shell to execute/run
	 */
	private String[] getCommands(String[] inputCommandsc, int curPos1, int curPos2)
	{
		//Check Array for semi-colons, and ampersands. Change position accordingly
		if((inputCommandsc[(curPos2 - 1)].equals(";")) || (inputCommandsc[(curPos2 - 1)].equals("&")))
			curPos2 -= 1; //Change position with our current int

		//Check for an out of bounds or null data to avoid crashes
		if(curPos2 - curPos1 <= 0)
			return null;
		
		//Init the return string after checks
		String[] cmdStrReturn = new String[curPos2 - curPos1]; 
		
		//Loop across input array to generate an array of our commands to execute.
		for(int i = curPos1; i < curPos2; i++)
			cmdStrReturn[(i - curPos1)] = inputCommandsc[i];
		
		return cmdStrReturn; //Output now shell usable
	}
}

